var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	// ...
	var ime = req.param("uporabniskoIme");
	var geslo = req.param("geslo");
	console.log(ime, geslo)

	var data = {status: "status", napaka: "Opis napake"};

	if(preveriSpomin(ime, geslo) || preveriDatotekaStreznik(ime, geslo)) {
		data = {};
	}
	else {
		data.status = "Napačna zahteva.";
	}

	res.send(data);
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	// ...
	var ime = req.param("uporabniskoIme");
	var geslo = req.param("geslo");
	console.log(ime, geslo)

	if(preveriSpomin(ime, geslo) || preveriDatotekaStreznik(ime, geslo)) {
		res.send("<html><title>Naslov strani</title><body><p>Uporabnik <b>"+ime+"</b> uspešno prijavljen v sistem!</p></body></html>");
	}
	else {
		res.send("<html><title>Naslov strani</title><body><p>Uporabnik <b>"+ime+"</b> nima pravice prijave v sistem!</p></body></html>");
	}

	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
var json = fs.readFileSync(__dirname + "/public/podatki/uporabniki_streznik.json").toString();
var podatkiDatotekaStreznik = JSON.parse(json);
console.log(podatkiDatotekaStreznik);

/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriSpomin(uporabniskoIme, geslo) {
	// ...
	for(var i = 0; i < podatkiSpomin.length; i++) {
		var data = podatkiSpomin[i].split('/');
		if(uporabniskoIme === data[0] && geslo === data[1])
			return true;
	}
	return false;
	// ...
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	// ...
	for(var i = 0; i < podatkiDatotekaStreznik.length; i++) {
		if(podatkiDatotekaStreznik[i].uporabnik == uporabniskoIme && podatkiDatotekaStreznik[i].geslo == geslo)
			return true;
	}

	return false;
	// ...
}